//
//  Annotation.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 16/02/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String) {
        self.coordinate = coordinate
        self.title = title
    }
}
