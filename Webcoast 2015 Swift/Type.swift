//
//  Type.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(Type)
class Type: NSManagedObject {

    @NSManaged var typeId: NSNumber
    @NSManaged var title: String
    @NSManaged var events: NSSet

}
