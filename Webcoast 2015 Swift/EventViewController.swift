//
//  EventViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 02/02/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit

class EventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var event: Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let event = self.event {
            self.title = event.title
            self.setFavoritedButtonImage()
        }
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    
    // MARK: - Actions
    
    func setFavoritedButtonImage() {
        if self.event?.favorited == true {
            favoriteButton.image = UIImage(named: "star-filled")
        } else {
            favoriteButton.image = UIImage(named: "star-outline")
        }
    }
    
    @IBAction func favoriteButtonPressed(sender: UIBarButtonItem) {
        var fav = true
        if let favs = self.event?.favorited {
            fav = favs ? false : true
        }
        self.event?.favorited = fav
        CoreDataManager.sharedManager.saveContext()
        self.setFavoritedButtonImage()
    }
    
    // MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.event?.eventDescription == "" {
            return 1
        }
        if self.event?.speakers?.count > 0 {
            return 3
        }
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 1
        case 2:
            return self.event!.speakers!.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Info"
        case 1:
            return "Innehåll"
        case 2:
            return "Talare"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath) as UITableViewCell
            let label1 = cell.contentView.viewWithTag(1) as! UILabel
            let label2 = cell.contentView.viewWithTag(2) as! UILabel
            switch indexPath.row {
            case 0:
                label1.text = "Vad:"
                label2.text = self.event?.type?.title
            case 1:
                label1.text = "Om:"
                label2.text = self.event?.subject?.title
            case 2:
                label1.text = "Var:"
                label2.text = self.event?.room?.title
            case 3:
                label1.text = "När:"
                if let event = self.event {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.locale = NSLocale.currentLocale()
                    dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
                    dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                    label2.text = dateFormatter.stringFromDate(event.startTime)
//                    if let endTime = self.event?.endTime {
//                        dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
//                        label2.text = label2.text! + " - " + dateFormatter.stringFromDate(endTime)
//                    }
                }
            default:
                break
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("ContentCell", forIndexPath: indexPath) as UITableViewCell
            let label = cell.contentView.viewWithTag(1) as! UILabel
            label.text = self.event?.eventDescription
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("SpeakerCell", forIndexPath: indexPath) as UITableViewCell
            if let speaker = self.event?.speakers?.allObjects[indexPath.row] as? Speaker {
                let label = cell.contentView.viewWithTag(1) as! UILabel
                label.text = speaker.name
                if speaker.speakerURLString != nil && speaker.speakerURLString != "" {
                    cell.selectionStyle = UITableViewCellSelectionStyle.Default
                } else {
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                }
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if indexPath.section == 2 {
            if let speaker = self.event?.speakers?.allObjects[indexPath.row] as? Speaker {
                if speaker.speakerURLString != nil && speaker.speakerURLString != "" {
                    print("Speaker URL is \(speaker.speakerURLString!)")
                }
            }
        }
    }
    
}
