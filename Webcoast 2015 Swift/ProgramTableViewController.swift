//
//  ProgramTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 22/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class ProgramTableViewController: BaseProgramTableViewController, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, ProgramTableViewCellDelegate {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var program = [[[Event]]]()
    var allSessions = [Event]()
    
    var searchController: UISearchController!
    var resultsTableViewcontroller = SearchedProgramTableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resultsTableViewcontroller.tableView.delegate = self
        searchController = UISearchController(searchResultsController: resultsTableViewcontroller)
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Sök"
        searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        
        self.segmentedControl.tintColor = UIColor.whiteColor()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.totalReload()
    }
    
    func totalReload() {
        // 1. Get Core Data
        self.program = self.getCoreDataProgram()
        // 2. Present Core Data
        self.tableView.reloadData()
        // 3. Get Remote data
        self.getRemoteProgram { response in
            // 4. Update Core Data
//            self.createCoreDataObjectsFromJSONResponse(response)
            self.createCoreDataObjectsFromJSONResponse(response, completion: { () -> () in
                // 5. Present Core Data
                self.program = self.getCoreDataProgram()
                self.tableView.reloadData()
            })
            // 5. Present Core Data
            self.program = self.getCoreDataProgram()
            self.tableView.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToEvent" {
            let eventViewController = segue.destinationViewController as! EventViewController
            let event = sender as! Event
            eventViewController.event = event
        }
    }
    
    // MARK: - Actions
    
    @IBAction func segmentedControlValueDidChange(sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
    
    // MARK: - Data Handling
    
    func getRemoteProgram(completion: (response: AnyObject?) -> ()) {
        Alamofire.request(.GET, "http://www.webcoast.se/program/json/")
            .responseJSON { (_, _, jsonResponse, error) in
                if (error != nil) {
                    println("Program Error: \(error!)")
                }
                completion(response: jsonResponse)
        }
    }
    
    func createCoreDataObjectsFromJSONResponse(jsonResponse: AnyObject?, completion: () -> ()) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            
            if let jsonResponse: AnyObject = jsonResponse {
                
                //2015-01-26 10:20:24 +0000
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzzz"
                
                let json = JSON(jsonResponse)
                
                var existingEventIds = [Int]()
                
                for (index, day) in json {
                    for (index, subDay) in json {
                        for (index, event) in subDay {
                            if let eventId = event["id"].int {
                                let title = event["title"].string
                                let eventDescription = event["content"].string
                                // Date
                                var startTime: NSDate!
                                if let startTimeString = event["start_time"].string {
                                    startTime = dateFormatter.dateFromString(startTimeString)
                                }
                                if startTime == nil {
                                    println("Start Time is nil")
                                    if let startDayString = event["date"].string {
                                        let dateFormatter2 = NSDateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd"
                                        startTime = dateFormatter.dateFromString(startDayString)
                                        if startTime == nil {
                                            println("Start time is still nil")
                                        }
                                    } else {
                                        println("No date")
                                    }
                                }
                                var endTime: NSDate?
                                if let endTimeString = event["end_time"].string {
                                    endTime = dateFormatter.dateFromString(endTimeString)
                                }
                                // Speakers
                                var speakers = NSMutableSet()
                                for (index, speaker) in event["speaker"] {
                                    let name = speaker["name"].string
                                    let urlString = speaker["url"].string
                                    if name != nil && name != "" {
                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                            let speaker = self.updateOrCreateSpeakerWithId(name!, urlString: urlString)
                                            if speaker != nil {
                                                speakers.addObject(speaker!)
                                            }
                                        })
                                    }
                                }
                                // Room
                                var room: Room?
                                let roomId = event["room"]["room_id"].int
                                let roomName = event["room"]["room_name"].string
                                if roomId != nil && roomName != nil && roomName! != "" {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        room = self.updateOrCreateRoomWithId(roomId!, name: roomName!)
                                    })
                                }
                                // Subject
                                var subject: Subject?
                                let subjectId = event["subject"]["subject_id"].int
                                let subjectName = event["subject"]["subject_name"].string
                                if subjectId != nil && subjectName != nil && subjectName! != "" {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        subject = self.updateOrCreateSubjectWithId(subjectId!, name: subjectName!)
                                    })
                                }
                                // Type
                                var type: Type?
                                let typeId = event["type"]["type_id"].int
                                let typeName = event["type"]["type_name"].string
                                if typeId != nil && typeName != nil && typeName! != "" {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        type = self.updateOrCreateTypeWithId(typeId!, name: typeName!)
                                    })
                                }
                                
                                let components = NSCalendar.currentCalendar().components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: startTime)
//                                if components.year == 2014 && components.month == 3 && (components.day == 14 || components.day == 15 || components.day == 16) { // 2014
                                if components.year == 2015 && components.month == 3 && (components.day == 13 || components.day == 14 || components.day == 15) { // 2015
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.updateOrCreateEventWithId(eventId, title: title, startTime: startTime, endTime: endTime, eventDescription: eventDescription, speakers: speakers, room: room, subject: subject, type: type)
                                        existingEventIds.append(eventId)
                                    })
                                }
                            } else {
                                println("Event is is nil")
                            }
                        }
                    }
                }
                
                // Clear all core data events that doesn't exist
                var existingEvents = self.getCoreDataEvents()
                var eventsToBeRemoved = [Event]()
                for event in existingEvents {
                    var exist = false
                    for id in existingEventIds {
                        if id == event.eventId {
                            exist = true
                        }
                    }
                    if exist == false {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            let voidMaybe: Void? = CoreDataManager.sharedManager.managedObjectContext?.deleteObject(event)
                        })
                    }
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let success = CoreDataManager.sharedManager.saveContext()
                    completion()
                })
            } else {
                print("Response is nil")
            }
        })
        
    }
    
    // MARK: - Core Data
    
    func getCoreDataEvents() -> [Event] {
        let fetchRequest = NSFetchRequest(entityName: "Event")
        var error: NSError?
        var events = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Event]
        return events
    }
    
    func getCoreDataProgram() -> [[[Event]]] {
        var events = self.getCoreDataEvents()
        events.sortInPlace { $0.startTime.timeIntervalSinceReferenceDate < $1.startTime.timeIntervalSinceReferenceDate }
        self.allSessions = events
        var tempProgram = [[[Event]]]()
        var tempProgramDay = [[Event]]()
        var tempProgramTime = [Event]()
        var lastDay: Int?
        var lastHour: Int?
        var lastMinute: Int?
        var favoritesDay = [[Event]]()
        var favoriteTime = [Event]()
        var lastFavoritesDay: Int?
        var lastFavoriteHour: Int?
        var lastFavoriteMinute: Int?
        for event in events {
            let components = NSCalendar.currentCalendar().components([.Day, .Hour, .Minute], fromDate: event.startTime)
            let currentDay = components.day
            let currentHour = components.hour
            let currentMinute = components.minute

            // New day
            if lastDay != nil && lastDay != currentDay {
                tempProgramDay.append(tempProgramTime)
                tempProgramTime = [Event]()
                tempProgram.append(tempProgramDay)
                tempProgramDay = [[Event]]()
            }
            
            if lastDay == currentDay && (lastHour != currentHour || lastMinute != currentMinute) {
                tempProgramDay.append(tempProgramTime)
                tempProgramTime = [Event]()
            }
            
            tempProgramTime.append(event)
            
            if event.favorited == true {
                if lastFavoritesDay != currentDay || lastFavoriteHour != currentHour || lastFavoriteMinute != currentMinute {
                    favoritesDay.append(favoriteTime)
                    favoriteTime = [Event]()
                }
                favoriteTime.append(event)
                lastFavoritesDay = currentDay
                lastFavoriteHour = currentHour
                lastFavoriteMinute = currentMinute
            }
            
            lastDay = currentDay
            lastHour = currentHour
            lastMinute = currentMinute
            
        }
        tempProgramDay.append(tempProgramTime)
        tempProgram.append(tempProgramDay)
        
        favoritesDay.append(favoriteTime)
        tempProgram.append(favoritesDay)
        
        return tempProgram
    }
    
    // MARK: Event
    
    func updateOrCreateEventWithId(id: Int, title: String?, startTime: NSDate, endTime: NSDate?, eventDescription: String?, speakers: NSSet?, room: Room?, subject: Subject?, type: Type?) -> Event? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var event = self.getEventFromCoreData(id)
        if event == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: managedObjectContext)
                event = Event(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        
        event?.eventId = id
        event?.title = String(NSString(string: title!).stringByDecodingHTMLEntities())
        event?.startTime = startTime
        event?.endTime = endTime
        event?.eventDescription = String(NSString(string: eventDescription!).stringByDecodingHTMLEntities())
        event?.speakers = speakers
        event?.room = room
        event?.subject = subject
        event?.type = type
        return event
    }
    
    func getEventFromCoreData(id: Int) -> Event? {
        let fetchRequest = NSFetchRequest(entityName: "Event")
        fetchRequest.predicate = NSPredicate(format: "eventId == \(id)", argumentArray: nil)
        var error: NSError?
        let events = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Event]?
        if let error = error {
            print(error)
        }
        if let event = events?.first {
            return event
        }
        return nil
    }

    // MARK: Speaker
    
    func updateOrCreateSpeakerWithId(name: String, urlString: String?) -> Speaker? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var speaker = self.getSpeakerFromCoreData(name)
        if speaker == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Speaker", inManagedObjectContext: managedObjectContext)
                speaker = Speaker(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        speaker?.name = name
        speaker?.speakerURLString = urlString
        return speaker
    }
    
    func getSpeakerFromCoreData(name: String) -> Speaker? {
        let fetchRequest = NSFetchRequest(entityName: "Speaker")
        fetchRequest.predicate = NSPredicate(format: "name == \'\(name)\'", argumentArray: nil)
        var error: NSError?
        let speakers = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Speaker]?
        if let error = error {
            print(error)
        }
        if let speaker = speakers?.first {
            return speaker
        }
        return nil
    }
    
    // MARK: Room
    
    func updateOrCreateRoomWithId(id: Int, name: String) -> Room? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var room = self.getRoomFromCoreData(id)
        if room == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Room", inManagedObjectContext: managedObjectContext)
                room = Room(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        room?.roomId = id
        room?.title = name
        return room
    }
    
    func getRoomFromCoreData(id: Int) -> Room? {
        let fetchRequest = NSFetchRequest(entityName: "Room")
        fetchRequest.predicate = NSPredicate(format: "roomId == \(id)", argumentArray: nil)
        var error: NSError?
        let rooms = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Room]?
        if let error = error {
            print(error)
        }
        if let room = rooms?.first {
            return room
        }
        return nil
    }
    
    // MARK: Subject
    
    func updateOrCreateSubjectWithId(id: Int, name: String) -> Subject? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var subject = self.getSubjectFromCoreData(id)
        if subject == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Subject", inManagedObjectContext: managedObjectContext)
                subject = Subject(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        subject?.subjectId = id
        subject?.title = name
        return subject
    }
    
    func getSubjectFromCoreData(id: Int) -> Subject? {
        let fetchRequest = NSFetchRequest(entityName: "Subject")
        fetchRequest.predicate = NSPredicate(format: "subjectId == \(id)", argumentArray: nil)
        var error: NSError?
        let subjects = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Subject]?
        if let error = error {
            print(error)
        }
        if let subject = subjects?.first {
            return subject
        }
        return nil
    }
    
    // MARK: Type
    
    func updateOrCreateTypeWithId(id: Int, name: String) -> Type? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var type = self.getTypeFromCoreData(id)
        if type == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Type", inManagedObjectContext: managedObjectContext)
                type = Type(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        type?.typeId = id
        type?.title = name
        return type
    }
    
    func getTypeFromCoreData(id: Int) -> Type? {
        let fetchRequest = NSFetchRequest(entityName: "Type")
        fetchRequest.predicate = NSPredicate(format: "typeId == \(id)", argumentArray: nil)
        var error: NSError?
        let types = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Type]?
        if let error = error {
            print(error)
        }
        if let type = types?.first {
            return type
        }
        return nil
    }
    
    // MARK: - Search Stuff
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchResults = self.allSessions
        // Strip out leading and trailing spaces
        let whitespaceCharacterSet = NSCharacterSet.whitespaceCharacterSet()
        let searchString = searchController.searchBar.text.stringByTrimmingCharactersInSet(whitespaceCharacterSet)
        
        // Separate search words
        let searchItems = searchString.componentsSeparatedByString(" ") as [String]
        
        // Build all the "AND" expressions for each value in the searchString.
        var andMatchPredicates = [NSPredicate]()
        
        for searchString in searchItems {
            // Each searchString creates an OR predicate for: name, twitter, company, website.
            var searchItemsPredicate = [NSPredicate]()
            // Below we use NSExpression represent expressions in our predicates.
            // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value).
            
            // title field matching.
            var lhs = NSExpression(forKeyPath: "title")
            var rhs = NSExpression(forConstantValue: searchString)
            var finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)
            
            // eventDescription field matching.
            lhs = NSExpression(forKeyPath: "eventDescription")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)
            
//            // room field matching.
//            lhs = NSExpression(forKeyPath: "room.title")
//            rhs = NSExpression(forConstantValue: searchString)
//            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
//            searchItemsPredicate.append(finalPredicate)

            // subject field matching.
            lhs = NSExpression(forKeyPath: "subject.title")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)
            
            // type field matching.
            lhs = NSExpression(forKeyPath: "type.title")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)
            
            // speaker field matching.
            lhs = NSExpression(forKeyPath: "speakers.name")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)
            
            // Add this OR predicate to our master AND predicate.
            let orMatchPredicates = NSCompoundPredicate.orPredicateWithSubpredicates(searchItemsPredicate)
            andMatchPredicates.append(orMatchPredicates)
        }
        
        // Match up the fields of the Product object.
        let finalCompoundPredicate = NSCompoundPredicate.andPredicateWithSubpredicates(andMatchPredicates)
        let filteredSessions = searchResults.filter { finalCompoundPredicate.evaluateWithObject($0) }
        
        let resultsController = searchController.searchResultsController as! SearchedProgramTableViewController
        resultsController.searchedEvents = filteredSessions
        resultsController.tableView.reloadData()
    }
    
    // MARK: - TableView
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.program.count == 0 {
            return 0
        }
        return self.program[self.segmentedControl.selectedSegmentIndex].count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let firstEvent = self.program[self.segmentedControl.selectedSegmentIndex][section].first as Event? {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale.currentLocale()
            dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            if segmentedControl.selectedSegmentIndex == 3 {
                dateFormatter.dateFormat = "EEEE HH:mm"
                return dateFormatter.stringFromDate(firstEvent.startTime)
            }
            return dateFormatter.stringFromDate(firstEvent.startTime)
        }
        return ""
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.program[self.segmentedControl.selectedSegmentIndex][section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.Cell.identifier, forIndexPath: indexPath) as! ProgramTableViewCell
        let event = self.program[segmentedControl.selectedSegmentIndex][indexPath.section][indexPath.row]
        cell.indexPath = indexPath
        cell.delegate = self
        configureCell(cell, forEvent: event, includeDayName: false)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var event: Event!
        if self.resultsTableViewcontroller.tableView.numberOfRowsInSection(0) > 0 {
            event = self.resultsTableViewcontroller.searchedEvents[indexPath.row]
        } else {
            event = self.program[segmentedControl.selectedSegmentIndex][indexPath.section][indexPath.row]
        }
        self.performSegueWithIdentifier("goToEvent", sender: event)
    }
    
    func programTableViewCellDidPressFavoriteButton(cell: ProgramTableViewCell, indexPath: NSIndexPath) {
        let event = self.program[segmentedControl.selectedSegmentIndex][indexPath.section][indexPath.row]
        event.favorited = event.favorited == true ? false : true
        CoreDataManager.sharedManager.saveContext()
        self.tableView.reloadData()
        self.totalReload()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.totalReload()
    }
}
