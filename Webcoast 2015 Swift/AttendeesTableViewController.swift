//
//  AttendeesViewController.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 22/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import RSTWebViewController

class AttendeesTableViewController: BaseAttendeeTableViewController, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, AttendeeTableViewCellDelegate {

    var attendees = [Attendee]()
    
    var searchController: UISearchController!
    var resultsTableViewcontroller = SearchedAttendeesTableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsTableViewcontroller.tableView.delegate = self
        searchController = UISearchController(searchResultsController: resultsTableViewcontroller)
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Sök"
        searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // 1. Get Core Data
        self.attendees = self.getCoreDataAttendees()
        // 2. Present Core Data
        self.tableView.reloadData()
        // 3. Get Remote data
        self.getRemoteAttendees { response in
            // 4. Update Core Data
//            self.createCoreDataObjectsFromJSONResponse(response)
            self.createCoreDataObjectsFromJSONResponse(response, completion: { () -> () in
                // 6. Present Core Data
                self.attendees = self.getCoreDataAttendees()
                self.tableView.reloadData()
            })
            // 5. Present Core Data
            self.attendees = self.getCoreDataAttendees()
            self.tableView.reloadData()
        }
    }
    
    // MARK: Data Handling
    
    func getRemoteAttendees(completion: (response: AnyObject?) -> ()) {
//        Alamofire.request(.GET, "https://www.eventbrite.com/json/event_list_attendees?app_key=SMK4GOFXTO4N72RL7F&user_key=138416022980267910481&id=2405484870") // 2014
        Alamofire.request(.GET, "https://www.eventbrite.com/json/event_list_attendees?app_key=SMK4GOFXTO4N72RL7F&user_key=138416022980267910481&id=14091934355") // 2015
            .responseJSON { (_, _, jsonResponse, error) in
                if (error != nil) {
                    println("Error: \(error)")
                }
                completion(response: jsonResponse)
        }
    }
    
    func createCoreDataObjectsFromJSONResponse(jsonResponse: AnyObject?, completion: () -> ()) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in

            if let jsonResponse: AnyObject = jsonResponse {
                let json = JSON(jsonResponse)
                if let attendees = json["attendees"].array {
                    for (index, subJSON) in json["attendees"] {
                        if let attendee = subJSON["attendee"].dictionary {
                            if let id = attendee["id"]?.int {
                                let firstName = attendee["first_name"]?.string
                                let lastName = attendee["last_name"]?.string
                                let company = attendee["company"]?.string
                                let website = attendee["website"]?.string
                                var name = ""
                                if firstName != nil && lastName != nil {
                                    name = "\(firstName!) \(lastName!)"
                                } else if firstName != nil && lastName == nil {
                                    name = firstName!
                                } else if firstName == nil && lastName != nil {
                                    name = lastName!
                                }
                                var twitter = ""
                                if let answers = attendee["answers"]?.array {
                                    for answer in answers {
                                        if let questionId = answer["answer"]["question_id"].int {
                                            if questionId == 8686937 {
                                                if let username = answer["answer"]["answer_text"].string {
                                                    twitter = "@" + username
                                                }
                                            }
                                        }
                                    }
                                }
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.updateOrCreateAttendeeWithId(id, name: name, company: company, twitter: twitter, website: website)
                                })
                            } else {
                                println("Attendee id is nil")
                            }
                        } else {
                            println("attendee is nil")
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let success = CoreDataManager.sharedManager.saveContext()
                        completion()
                    })
                } else {
                    print("attendees is nil")
                }
            } else {
                print("Response is nil")
            }
        });
    }

    // MARK: - Core Data
    
    func updateOrCreateAttendeeWithId(id: Int, name: String?, company: String?, twitter: String?, website: String?) {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var attendee = self.getAttendeeFromCoreData(id)
        if attendee == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("Attendee", inManagedObjectContext: managedObjectContext)
                attendee = Attendee(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        attendee!.attendeeId = id
        attendee!.name = name
        attendee!.company = company
        attendee!.twitter = twitter
        attendee!.website = website
    }
    
    func getAttendeeFromCoreData(id: Int) -> Attendee? {
        let fetchRequest = NSFetchRequest(entityName: "Attendee")
        fetchRequest.predicate = NSPredicate(format: "attendeeId == \(id)", argumentArray: nil)
        var error: NSError?
        let attendees = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Attendee]?
        if let error = error {
            print(error)
        }
        if let attendee = attendees?.first {
            return attendee
        }
        return nil
    }
    
    func getCoreDataAttendees() -> [Attendee] {
        let fetchRequest = NSFetchRequest(entityName: "Attendee")
        var error: NSError?
        var posts = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Attendee]
        if let error = error {
            print(error)
        }
        posts.sort { return $0.name < $1.name }
        return posts
    }
    
    // MARK: Search Stuff
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchResults = self.attendees
        
        // Strip out leading and trailing spaces
        let whitespaceCharacterSet = NSCharacterSet.whitespaceCharacterSet()
        let searchString = searchController.searchBar.text.stringByTrimmingCharactersInSet(whitespaceCharacterSet)
        
        // Separate search words
        let searchItems = searchString.componentsSeparatedByString(" ") as [String]
        
        // Build all the "AND" expressions for each value in the searchString.
        var andMatchPredicates = [NSPredicate]()
        
        for searchString in searchItems {
            // Each searchString creates an OR predicate for: name, twitter, company, website.
            var searchItemsPredicate = [NSPredicate]()
            // Below we use NSExpression represent expressions in our predicates.
            // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value).
            
            // name field matching.
            var lhs = NSExpression(forKeyPath: "name")
            var rhs = NSExpression(forConstantValue: searchString)
            var finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)

            // twitter field matching.
            lhs = NSExpression(forKeyPath: "twitter")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)

            // company field matching.
            lhs = NSExpression(forKeyPath: "company")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)

            // website field matching.
            lhs = NSExpression(forKeyPath: "website")
            rhs = NSExpression(forConstantValue: searchString)
            finalPredicate = NSComparisonPredicate(leftExpression: lhs, rightExpression: rhs, modifier: .DirectPredicateModifier, type: .ContainsPredicateOperatorType, options: .CaseInsensitivePredicateOption)
            searchItemsPredicate.append(finalPredicate)

            
            // Add this OR predicate to our master AND predicate.
            let orMatchPredicates = NSCompoundPredicate.orPredicateWithSubpredicates(searchItemsPredicate)
            andMatchPredicates.append(orMatchPredicates)
        }
        
        // Match up the fields of the Product object.
        let finalCompoundPredicate = NSCompoundPredicate.andPredicateWithSubpredicates(andMatchPredicates)
        let filteredAttendees = searchResults.filter { finalCompoundPredicate.evaluateWithObject($0) }
        
        let resultsController = searchController.searchResultsController as! SearchedAttendeesTableViewController
        resultsController.searchedAttendees = filteredAttendees
        resultsController.tableView.reloadData()
    }
    
    // MARK: TableView
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.attendees.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.Cell.identifier, forIndexPath: indexPath) as! AttendeeTableViewCell
        cell.delegate = self
        let attendee = self.attendees[indexPath.row]
        configureCell(cell, forAttendee: attendee)
        return cell
    }
    
    func attendeeTableViewCellDidPressedButton(button: UIButton) {
        if let website = button.titleLabel?.text {
            let webViewController = RSTWebViewController(address: website)
            webViewController.showsDoneButton = true
            let navigationController = UINavigationController(rootViewController: webViewController)
            presentViewController(navigationController, animated: true, completion: nil)
        }
    }
}
