//
//  Subject.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(Subject)
class Subject: NSManagedObject {

    @NSManaged var subjectId: NSNumber
    @NSManaged var title: String
    @NSManaged var events: NSSet

}
