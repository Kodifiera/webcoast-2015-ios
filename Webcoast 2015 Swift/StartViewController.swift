//
//  FirstViewController.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class StartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var suggestSession = false
    var sessions = [Event]()
    var titleString = "Välkommen!"
    var statusString = "Vi önskar dig varmt välkommen till WebCoast 2015!"
//    var debugDate: NSDate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsetsMake(-59, 0, 20, 0)
        
//        let dateComponents = NSCalendar.currentCalendar().components((.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay | .CalendarUnitHour | .CalendarUnitMinute), fromDate: NSDate())
//        dateComponents.year = 2014
//        dateComponents.month = 3
//        dateComponents.day = 14
//        dateComponents.hour = 13
//        dateComponents.minute = 12
//        debugDate = NSCalendar.currentCalendar().dateFromComponents(dateComponents)
        
        NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: Selector("updateUI"), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
        self.updateUI()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToSession" {
            let eventViewController = segue.destinationViewController as! EventViewController
            let event = sender as! Event
            eventViewController.event = event
        }
    }
    
    func updateUI() {
        self.sessions = self.getNextSessions(true)
        if self.sessions.count == 0 {
            self.suggestSession = true
            self.sessions = self.getNextSessions()
        } else {
            self.suggestSession = false
        }
        self.tableView.reloadData()
        
        self.getStatus { (title, status) -> () in
            self.titleString = title
            self.statusString = status
            self.tableView.reloadData()
        }
    }
    
    func getStatus(completion: (title: String, status: String) -> ()) {
//        "http://www.webcoast.se/api/get_posts/?post_type=wc_notis"
        Alamofire.request(.GET, "http://www.webcoast.se/api/get_posts/?post_type=wc_notis")
            .responseJSON { (_, _, response, error) in
                if let jsonResponse: AnyObject = response {
                    
                    let json = JSON(jsonResponse)
                    let title = json["posts"][0]["title"].string ?? ""
                    let status = json["posts"][0]["content"].string ?? ""
                    completion(title: String(NSString(string: title).stringByConvertingHTMLToPlainText()), status: String(NSString(string: status).stringByConvertingHTMLToPlainText()))
                }
        }
    }
    
    func getSessions() -> [Event] {
        let fetchRequest = NSFetchRequest(entityName: "Event")
        var error: NSError?
        var events = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [Event]
        events.sort { $0.startTime.timeIntervalSinceReferenceDate < $1.startTime.timeIntervalSinceReferenceDate }
        return events
    }
    
    func getNextSessions(onlyFavorites: Bool = false) -> [Event] {
        let events = self.getSessions()
//        let currentDate = self.debugDate
        let currentDate = NSDate()
        var favoriteSessions = [Event]()
        for event in events {
            let timeInterval = event.startTime.timeIntervalSinceDate(currentDate)
            if timeInterval > 0 {
                if onlyFavorites == false || (onlyFavorites == true && event.favorited) {
                    let timeLeft = self.getHourAndMinutesUntilEvent(event)
//                    if timeLeft.hour < 99 {
                    if onlyFavorites == true || (onlyFavorites == false && (event.type?.typeId == 70 || event.type?.typeId == 86)) {
                        favoriteSessions.append(event)
                    }
//                    }
                }
            }
        }
        return favoriteSessions
    }
    
    func getHourAndMinutesUntilEvent(event: Event) -> (hour: Int, minutes: Int) {
//        let currentDate = self.debugDate
        let currentDate = NSDate()
        let timeInterval = event.startTime.timeIntervalSinceDate(currentDate)
        if timeInterval <= 0 {
            return (0, 0)
        }
        let hours = Int(timeInterval / 60 / 60)
        let minutes = Int((timeInterval / 60) % 60)
        return (hours, minutes)
    }
        
    // MARK: - TabelView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            let count = self.sessions.count
            if count > 2 {
                return 2
            }
            return count
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            if self.suggestSession {
                return 80
            } else {
                return 40
            }
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRectMake(0, 0, tableView.bounds.width, 40))
        label.textAlignment = .Center
        label.font = UIFont.boldSystemFontOfSize(17)
        label.numberOfLines = 0
        if self.suggestSession {
            if self.sessions.count == 0 {
                label.text = ""
            } else {
                label.text = "Du har inga sparade sessions till ditt favoritprogram. Kanske kan någon av de här intressera dig?"
            }
        } else {
            label.text = "Dina kommande favoritsessioner"
        }
        return label
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as UITableViewCell
            let titleLabel = cell.contentView.viewWithTag(1) as! UILabel
            let statusLabel = cell.contentView.viewWithTag(2) as! UILabel
            titleLabel.text = titleString
            statusLabel.text = statusString
            return cell
        default:
            let event = self.sessions[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
            let behindTimeView = cell.contentView.viewWithTag(1) as UIView!
            let timeLabel = cell.contentView.viewWithTag(2) as! UILabel!
            let roomLabel = cell.contentView.viewWithTag(3) as! UILabel!
            let descriptionLabel = cell.contentView.viewWithTag(4) as! UILabel!
            if let roomTitle = event.room?.title {
                roomLabel.text = "Rum: " + roomTitle
            } else {
                roomLabel.text = "Rum: Okänt"
            }
            descriptionLabel.text = event.title
            let timeLeft = self.getHourAndMinutesUntilEvent(event)
            if timeLeft.hour == 0 {
                timeLabel.text = "\(timeLeft.minutes)min"
            } else {
                timeLabel.text = "\(timeLeft.hour)h \(timeLeft.minutes)min"
            }
            switch indexPath.row {
            case 0:
                behindTimeView.backgroundColor = UIColor(red:0.73, green:0.17, blue:0.14, alpha:1)
                timeLabel.textColor = UIColor.whiteColor()
            default:
                behindTimeView.backgroundColor = UIColor.whiteColor()
                timeLabel.textColor = UIColor(red:0.73, green:0.17, blue:0.14, alpha:1)
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if indexPath.section == 1 {
            let event = self.sessions[indexPath.row]
            performSegueWithIdentifier("goToSession", sender: event)
        }
    }
}

