//
//  ProgramTableViewCell.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 22/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit

protocol ProgramTableViewCellDelegate {
    func programTableViewCellDidPressFavoriteButton(cell: ProgramTableViewCell, indexPath: NSIndexPath)
}

class ProgramTableViewCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    
    var delegate: ProgramTableViewCellDelegate?
    var indexPath: NSIndexPath?
    
    @IBAction func starButtonPressed(sender: UIButton) {
        if let indexPath = self.indexPath {
            self.delegate?.programTableViewCellDidPressFavoriteButton(self, indexPath: indexPath)
        } else {
            print("event is nil")
        }
    }
    
    func setFavorite(favorite: Bool) {
        if favorite {
//            self.starButton.imageView?.image = UIImage(named: "star-filled")
            self.starButton.setImage(UIImage(named: "star-filled"), forState: .Normal)
        } else {
//            self.starButton.imageView?.image = UIImage(named: "star-outline")
            self.starButton.setImage(UIImage(named: "star-outline"), forState: .Normal)
        }
    }
}
