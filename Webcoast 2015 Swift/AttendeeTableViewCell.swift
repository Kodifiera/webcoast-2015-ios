//
//  AttendeeTableViewCell.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 06/11/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit

protocol AttendeeTableViewCellDelegate {
    func attendeeTableViewCellDidPressedButton(button: UIButton)
}

class AttendeeTableViewCell: UITableViewCell {
    
    var delegate: AttendeeTableViewCellDelegate?

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var twitterLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    
    @IBAction func websiteButtonDidTouchUpInside(sender: UIButton) {
        self.delegate?.attendeeTableViewCellDidPressedButton(sender)
    }

}
