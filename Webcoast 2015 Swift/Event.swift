//
//  Event.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(Event)
class Event: NSManagedObject {

    @NSManaged var title: String?
    @NSManaged var eventId: NSNumber?
    @NSManaged var startTime: NSDate
    @NSManaged var favorited: Bool
    @NSManaged var endTime: NSDate?
    @NSManaged var eventDescription: String?
    @NSManaged var speakers: NSSet?
    @NSManaged var room: Room?
    @NSManaged var subject: Subject?
    @NSManaged var type: Type?

}
