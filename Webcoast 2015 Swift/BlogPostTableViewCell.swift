//
//  BlogPostTableViewCell.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit

class BlogPostTableViewCell: UITableViewCell {

    @IBOutlet weak var blogPostImageView: UIImageView!
    @IBOutlet weak var blogPostTitle: UILabel!
    
}
