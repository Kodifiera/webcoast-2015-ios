//
//  BlogPost.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(BlogPost)
class BlogPost: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var title: String?
    @NSManaged var imageURLString: String?
    @NSManaged var content: String?
    @NSManaged var imageData: NSData?
    @NSManaged var date: NSDate

}
