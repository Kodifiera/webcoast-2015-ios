//
//  BaseAttendeeTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 21/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit

class BaseAttendeeTableViewController: UITableViewController {

    struct Constants {
        struct Nib {
            static let name = "AttendeeTableViewCell"
        }
        struct Cell {
            static let identifier = "AttendeeCell"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: Constants.Nib.name, bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: Constants.Cell.identifier)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 136
    }

    func configureCell(cell: AttendeeTableViewCell, forAttendee attendee: Attendee) {
        cell.nameLabel.text = attendee.name
        cell.twitterLabel.text = attendee.twitter
        cell.companyLabel.text = attendee.company
        cell.websiteButton.setTitle(attendee.website, forState: .Normal)
    }
}
