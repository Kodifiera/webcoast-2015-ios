//
//  ThemeKit.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 22/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit

class ThemeKit: NSObject {
    
    enum ThemeKitType : Int {
        case TabBar
    }
    
    class func theme(primaryColor: UIColor, secondaryColor: UIColor, fontName: String, lightStatusBar: Bool) {
        if lightStatusBar {
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        }
        
        self.customizeNavigationBar(barColor: primaryColor, textColor: secondaryColor, buttonColor: secondaryColor, fontName: fontName, fontSize: 22)
        self.customizeNavigationBarButton(color: secondaryColor)
        self.customizeTabBar(barColor: secondaryColor, textColor: primaryColor, fontName: fontName, fontSize: 12)
        self.customizeSwitchOn(color: primaryColor)
        self.customizeSearchBar(barColor: primaryColor, tintColor: secondaryColor)
        self.customizeActivityIndicator(color: primaryColor)
        self.customizeButton(primaryColor)
        self.customizeSegmentedControl(mainColor: primaryColor, secondaryColor: secondaryColor)
        self.customizeSlider(color: primaryColor)
        self.customizePageControl(currentPageColor: primaryColor)
        self.customizeToolbar(color: primaryColor)
    }
    
    class func swithPrimaryAndSecondaryColorsFor(type: ThemeKitType) {
        switch type {
        case ThemeKitType.TabBar:
            let tintColor = UITabBar.appearance().tintColor
            let barTintColor = UITabBar.appearance().barTintColor
            UITabBar.appearance().tintColor = barTintColor
            UITabBar.appearance().barTintColor = tintColor
            break
        default:
            break
        }
        
    }
    
    // MARK: - UINavigationBar
    
    class func customizeNavigationBar(barColor barColor: UIColor, textColor: UIColor, buttonColor: UIColor) {
        UINavigationBar.appearance().barTintColor = barColor
        UINavigationBar.appearance().tintColor = buttonColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : textColor]
    }
    
    class func customizeNavigationBar(barColor barColor: UIColor, textColor: UIColor, buttonColor: UIColor, fontName: String, fontSize: Float) {
        UINavigationBar.appearance().barTintColor = barColor
        UINavigationBar.appearance().tintColor = buttonColor
        let font: UIFont? = UIFont(name: fontName, size: CGFloat(fontSize))
        if let font = font {
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : textColor, NSFontAttributeName : font]
        } else {
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : textColor]
        }
    }
    
    // MARK: - UIBarButtonItem
    
    class func customizeNavigationBarButton(color color: UIColor) {
        //        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : color], forState: UIControlState.Normal)
    }
    
    // MARK: - UITabBar
    
    class func customizeTabBar(barColor barColor: UIColor, textColor: UIColor) {
        UITabBar.appearance().barTintColor = barColor
        UITabBar.appearance().tintColor = textColor
    }
    
    class func customizeTabBar(barColor barColor: UIColor, textColor: UIColor, fontName: String, fontSize: Float) {
        UITabBar.appearance().barTintColor = barColor
        UITabBar.appearance().tintColor = textColor
        let font: UIFont? = UIFont(name: fontName, size: CGFloat(fontSize))
        if let font = font {
            UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName : font], forState: UIControlState.Normal)
        }
    }
    
    // MARK: - UIButton
    
    class func customizeButton(color: UIColor) {
        UIButton.appearance().setTitleColor(color, forState: UIControlState.Normal)
        UIButton.appearance().tintColor = color
    }
    
    // MARK: - UISwitch
    
    class func customizeSwitchOn(color color: UIColor) {
        UISwitch.appearance().onTintColor = color
    }
    
    // MARK: - UISearchBar
    
    class func customizeSearchBar(barColor barColor: UIColor, tintColor: UIColor) {
        UISearchBar.appearance().barTintColor = barColor
        UISearchBar.appearance().tintColor = tintColor
    }
    
    // MARK: - UIActivityIndicator
    
    class func customizeActivityIndicator(color color: UIColor) {
        UIActivityIndicatorView.appearance().color = color
    }
    
    // MARK: - UISegmentedControl
    
    class func customizeSegmentedControl(mainColor mainColor: UIColor, secondaryColor: UIColor) {
        UISegmentedControl.appearance().tintColor = mainColor
    }
    
    // MARK: - UISlider
    
    class func customizeSlider(color color: UIColor) {
        UISlider.appearance().minimumTrackTintColor = color
    }
    
    // MARK: - UIToolbar
    class func customizeToolbar(color color: UIColor) {
        UIToolbar.appearance().tintColor = color
    }
    
    // MARK: UIPageControl
    class func customizePageControl(currentPageColor currentPageColor: UIColor, otherPageColor: UIColor) {
        UIPageControl.appearance().pageIndicatorTintColor = otherPageColor
        UIPageControl.appearance().currentPageIndicatorTintColor = currentPageColor
        UIPageControl.appearance().backgroundColor = UIColor.clearColor()
    }
    
    class func customizePageControl(currentPageColor currentPageColor: UIColor) {
        self.customizePageControl(currentPageColor: currentPageColor, otherPageColor: UIColor.lightGrayColor())
    }
    
    // MARK: Utilities
    class func color(red red: Float, green: Float, blue:Float) -> UIColor {
        return self.color(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func color(red red: Float, green: Float, blue:Float, alpha: Float) -> UIColor {
        let r = red / 255.0
        let g = green / 255.0
        let b = blue / 255.0
        return UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: CGFloat(alpha))
    }
}