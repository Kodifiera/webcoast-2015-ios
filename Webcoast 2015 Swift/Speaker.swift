//
//  Speaker.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(Speaker)
class Speaker: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var speakerURLString: String?
    @NSManaged var events: NSSet

}
