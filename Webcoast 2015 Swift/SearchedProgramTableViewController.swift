//
//  SearchedProgramTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 22/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit

class SearchedProgramTableViewController: BaseProgramTableViewController, ProgramTableViewCellDelegate {

    var searchedEvents = [Event]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchedEvents.count
    }
        
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.Cell.identifier) as! ProgramTableViewCell
        cell.indexPath = indexPath
        cell.delegate = self
        let event = self.searchedEvents[indexPath.row]
        configureCell(cell, forEvent: event, includeDayName: true)
        return cell
    }
    
    func programTableViewCellDidPressFavoriteButton(cell: ProgramTableViewCell, indexPath: NSIndexPath) {
        let event = self.searchedEvents[indexPath.row]
        event.favorited = event.favorited == true ? false : true
        CoreDataManager.sharedManager.saveContext()
        self.tableView.reloadData()
    }
}
