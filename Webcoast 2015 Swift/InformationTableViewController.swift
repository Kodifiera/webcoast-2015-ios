//
//  InformationTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 16/02/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit
import MapKit
import RSTWebViewController

class InformationTableViewController: UITableViewController, UIActionSheetDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
//        self.tableView.estimatedRowHeight = 120
        
        let annotationCoordinate = CLLocationCoordinate2D(latitude: 57.707126, longitude: 11.939796)
        let annotation = Annotation(coordinate: annotationCoordinate, title: "WebCoast")

//        let center = CLLocationCoordinate2D(latitude: 57.708895, longitude: 11.973479)
        let center = CLLocationCoordinate2D(latitude: 57.708, longitude: 11.96)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(center, span)
        self.mapView.region = region
        self.mapView.addAnnotation(annotation)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "mapViewTapped:")
        self.mapView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.tableView.reloadData()
    }
    
    func mapViewTapped(sender: UITapGestureRecognizer) {
        let actionSheet = UIActionSheet(title: "Vill du öppna kartappen?", delegate: self, cancelButtonTitle: "Avbryt", destructiveButtonTitle: nil, otherButtonTitles: "Ja")
        actionSheet.tag = 6
        actionSheet.showInView(self.view)
    }
    
    
    @IBAction func moreInfoButtonPressed(sender: UIButton) {
        let webViewController = RSTWebViewController(address: "http://www.webcoast.se/om-webcoast/")
        webViewController.showsDoneButton = true
        let navigationController = UINavigationController(rootViewController: webViewController)
        presentViewController(navigationController, animated: true, completion: nil)
    }
    
    //MARK: - Actionsheet
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        print(buttonIndex)
        
        if actionSheet.tag == 6 && buttonIndex == 1 {
            let annotationCoordinate = CLLocationCoordinate2D(latitude: 57.707126, longitude: 11.939796)
            let placemark = MKPlacemark(coordinate: annotationCoordinate, addressDictionary: nil)
            let endingItem = MKMapItem(placemark: placemark)
            endingItem.openInMapsWithLaunchOptions(nil)
        } else {
        
        var email = ""
        var phone = ""
        switch actionSheet.tag {
        case 1:
            email = "maria.gustafsson@webcoast.se"
            phone = "0702707295"
        case 2:
            email = "szofia.jakobsson@webcoast.se"
            phone = "0737206464"
        case 3:
            email = "cedrik.sjoblom@webcoast.se"
            phone = "0700917719"
        case 4:
            email = "stefan@waborg.se"
            phone = "0707500113"
        case 5:
            email = "mattias.wahlberg@gmail.com"
            phone = "0734270787"
        default:
            break
        }
        
        switch buttonIndex {
        case 1:
            UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(email)")!)
        case 2:
            UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(email)")!)
        default:
            break
        }
        }
    }
    
    //MARK: - Contacts
    
    func openContactInformation(title: String, email: String, phone: String, tag: Int) {
        let actionSheet = UIActionSheet(title: "Hur vill du kontakta \(title)?", delegate: self, cancelButtonTitle: "Avbryt", destructiveButtonTitle: nil, otherButtonTitles: "Email: \(email)", "Telefon: \(phone)")
        actionSheet.tag = tag
        actionSheet.showInView(self.view)
    }
    
    @IBAction func maria(sender: UIButton) {
        self.openContactInformation("projektledare Maria Gustafsson", email: "maria.gustafsson@webcoast.se", phone: "070-270 72 95", tag: 1)
    }
    
    @IBAction func szofia(sender: UIButton) {
        self.openContactInformation("marknadsföringsansvarig Szofia Jakobsson", email: "szofia.jakobsson@webcoast.se", phone: "073-720 64 64", tag: 2)
    }
    
    @IBAction func cedrik(sender: UIButton) {
        self.openContactInformation("platsansvarig Cedrik Sjöblom", email: "cedrik.sjoblom@webcoast.se", phone: "070-091 77 19", tag: 3)
    }
    
    @IBAction func stefan(sender: UIButton) {
        self.openContactInformation("videoansvarig Stefan Waborg", email: "stefan@waborg.se", phone: "070-750 01 13", tag: 4)
    }
    
    @IBAction func mattias(sender: UIButton) {
        self.openContactInformation("hackathonansvarig Mattias Wahlberg", email: "mattias.wahlberg@gmail.com", phone: "073-427 07 87", tag: 5)
    }
}




