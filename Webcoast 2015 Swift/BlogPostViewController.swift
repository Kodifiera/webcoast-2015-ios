//
//  BlogPostViewController.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RSTWebViewController

class BlogPostViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    var blogPost: BlogPost?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let blogPost = self.blogPost {
            self.title = blogPost.title
            if let content = blogPost.content {
                self.loadContent()
            } else {
                // Fetch and save content
                self.getBlogPostContent { content in
                    if (content != nil) {
                        self.blogPost?.content = content
                        self.loadContent()
                        CoreDataManager.sharedManager.saveContext()
                    }
                }
            }
        }
    }
    
    func getBlogPostContent(completion: (content: String?) -> ()) {
        Alamofire.request(.GET, "http://www.webcoast.se/api/get_post/", parameters: ["id":self.blogPost!.id])
            .responseJSON { (_, _, jsonObject, error) in
                var content: String?
                if (error != nil) {
                    println("Error: \(error)")
                }
                if let jsonObject: AnyObject = jsonObject {
                    let json = JSON(jsonObject)
                    content = json["post"]["content"].string
                }
            completion(content: content)
        }
    }
    
    func loadContent() {
        let path = NSBundle.mainBundle().bundlePath
        let baseURL = NSURL.fileURLWithPath(path)
        let htmlString = "<html><header><link href=\"stylesheet.css\" rel=\"stylesheet\" type=\"text/css\"></header><body><img src=\"\(self.blogPost!.imageURLString!)\"><br />\(self.blogPost!.content!)</body></html>"
        self.webView.loadHTMLString(htmlString, baseURL: baseURL)
    }
    
    // MARK: - Web View

    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == .LinkClicked {
            let webViewController = RSTWebViewController(address: request.URLString)
            webViewController.showsDoneButton = true
            let navigationController = UINavigationController(rootViewController: webViewController)
            presentViewController(navigationController, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print(error)
    }
}
