//
//  BaseProgramTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 22/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit

class BaseProgramTableViewController: UITableViewController {

    struct Constants {
        struct Nib {
            static let name = "ProgramTableViewCell"
        }
        struct Cell {
            static let identifier = "ProgramCell"
        }
    }
    
    let dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: Constants.Nib.name, bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: Constants.Cell.identifier)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 62
    }
        
    func configureCell(cell: ProgramTableViewCell, forEvent event: Event, includeDayName: Bool) {
        cell.titleLabel.text = event.title
        dateFormatter.dateFormat = "HH:mm"
        cell.timeLabel.text = dateFormatter.stringFromDate(event.startTime)
        if event.endTime != nil {
            cell.timeLabel.text = cell.timeLabel.text! + " - " + dateFormatter.stringFromDate(event.endTime!)
        }
        if includeDayName && cell.timeLabel != nil{
            dateFormatter.dateFormat = "EEEE"
            cell.timeLabel.text = dateFormatter.stringFromDate(event.startTime) + " " + cell.timeLabel.text!
        }
        
        cell.setFavorite(event.favorited)
        
        var color = UIColor.whiteColor()
        if let typeId = event.type?.typeId {
            switch typeId {
            case 70:
                // Session
//                color = UIColor(red:0.88, green:0.28, blue:0.36, alpha:1)
                color = UIColor(red:0.73, green:0.17, blue:0.14, alpha:1)
            case 71:
                //Paus
                color = UIColor(red:0.37, green:1, blue:0.88, alpha:1)
            case 72:
                // Socialt
                color = UIColor(red:0.65, green:0.93, blue:0.25, alpha:1)
            case 85:
                // Mat
                color = UIColor(red:0.98, green:0.76, blue:0.33, alpha:1)
            case 86:
                // Workshop
                color = UIColor(red:0.39, green:0, blue:0.99, alpha:1)
            default:
                break
            }
        }
        cell.colorView.backgroundColor = color
        
    }
}
