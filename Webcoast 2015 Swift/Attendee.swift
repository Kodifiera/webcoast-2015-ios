//
//  Attendee.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 22/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import Foundation
import CoreData

@objc(Attendee)
class Attendee: NSManagedObject {

    @NSManaged var attendeeId: NSNumber
    @NSManaged var name: String?
    @NSManaged var twitter: String?
    @NSManaged var company: String?
    @NSManaged var website: String?
    @NSManaged var imageURLString: String?
    @NSManaged var imageData: NSData?

}
