//
//  BlogViewController.swift
//  Webcoast 2015 Swift
//
//  Created by Peter on 18/10/14.
//  Copyright (c) 2014 Peter Larsson. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class BlogTableViewController: UITableViewController, UISplitViewControllerDelegate {

    var blogPosts: [BlogPost] = []
    var currentPage = 1
    var dateFormatter = NSDateFormatter()
    var isFetching = false
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.splitViewController?.delegate = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: Selector("refreshControlValueDidChange:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let refreshControl = self.refreshControl {
            self.refreshControlValueDidChange(refreshControl)
        }
    }
    
    // MARK: - Data Handling
    
    func refreshControlValueDidChange(refreshControl: UIRefreshControl) {
        if (!refreshControl.refreshing) {
            refreshControl.beginRefreshing()
        }
        
        // 1. Get Core Data
        // 2. Present Core Data
        getCoreDataBlogPosts { (blogPosts) -> () in
            if let blogPosts = blogPosts {
                self.blogPosts = blogPosts
                self.tableView.reloadData()
            } else {
                print("Core data did not return blog posts")
            }
        }
        
        // 3. Fetch remote posts from page 1.
        self.currentPage = 1
        // 4. Loop and create posts if not exists.
        
        if (self.isFetching == true) {
            refreshControl.endRefreshing()
            return
        }
        
        getRemoteBlogPosts { response in
            self.createCoreDataObjectsFromJSONResponse(response)
            refreshControl.endRefreshing()
        }
    }
    
    func getRemoteBlogPosts(completion: (response: AnyObject?) -> ()) {
        if (self.isFetching == true) {
            return
        }
        self.isFetching = true
        Alamofire.request(.GET, "http://www.webcoast.se/api/get_recent_posts/", parameters: ["page":self.currentPage])
            .responseJSON { (_, _, jsonResponse, error) in
                if (error != nil) {
                    println("Error: \(error)")
                }
                completion(response: jsonResponse)
                self.currentPage++
                self.isFetching = false
        }
    }
    
    func createCoreDataObjectsFromJSONResponse(jsonResponse: AnyObject?) {
        if jsonResponse == nil {
            return
        }
        self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        let json = JSON(jsonResponse!)
        for (index, subJson) in json["posts"] {
            if let id = subJson["id"].int {
                var title: String?
                if let jsonTitle = subJson["title"].string {
                    title = String(NSString(string: jsonTitle).stringByDecodingHTMLEntities())
                }
                let imageURLString = subJson["thumbnail_images"]["frontpage-blog-post"]["url"].string
                if subJson["date"].string == nil {
                    continue
                }
                if let date = self.dateFormatter.dateFromString(subJson["date"].string!) {
//                    self.createBlogPost(id, title: title?, imageURLString: imageURLString?, content: nil, date: date)
                    self.updateOrCreateBlogPostWithId(id, title: title, imageURLString: imageURLString, content: nil, date: date)
                }
            }
        }
        
        self.getCoreDataBlogPosts({ (blogPosts) -> () in
            if blogPosts != nil {
                self.blogPosts = blogPosts!
                self.tableView.reloadData()
            }
            
        })
    }
    
    // MARK: - Core Data
    
//    func createBlogPost(id :Int, title: String?, imageURLString: String?, content: String?, date: NSDate) {
//        // If id doesn't exist - Create new blogPost
//        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//            self.blogPostExist(id, completion: { exists in
//                if (!exists) {
//                    println("blog post with id \(id) doesn't exist")
//                    
//                    if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
//                        let entity = NSEntityDescription.entityForName("BlogPost", inManagedObjectContext: managedObjectContext)
//                        var blogPost = BlogPost(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
//                        blogPost.blogPostId = NSNumber(integer: id)
//                        blogPost.title = title
//                        blogPost.imageURLString = imageURLString
//                        blogPost.date = date
//                        println("Created blogPost: \(blogPost)")
//                        var error: NSError?
//                        if CoreDataManager.sharedManager.saveContext() {
//                            self.blogPosts.append(blogPost)
//                            self.tableView.reloadData()
//                        }
//                    } else {
//                        println("No ManagedObjectContext")
//                    }
//                } else {
//                    println("Blog post with id \(id) already exists")
//                }
//            })
//        })
//    }
//
//    func blogPostExist(id: Int, completion: (Bool) -> ()) {
//        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//            let fetchRequest = NSFetchRequest(entityName: "BlogPost")
//            fetchRequest.predicate = NSPredicate(format: "blogPostId == \(id)", argumentArray: nil)
//            var error: NSError?
//            let count = CoreDataManager.sharedManager.managedObjectContext?.countForFetchRequest(fetchRequest, error: &error)
//            if let error = error {
//                println(error)
//            }
//            if (count > 0) {
//                completion(true)
//            } else {
//                completion(false)
//            }
//        })
//    }
    
    func updateOrCreateBlogPostWithId(id: Int, title: String?, imageURLString: String?, content: String?, date: NSDate) -> BlogPost? {
        // If id exist => update
        // If id doesn't exist => Create new blogPost
        var blogPost = self.getBlogPostFromCoreData(id)
        if blogPost == nil {
            // Create
            if let managedObjectContext = CoreDataManager.sharedManager.managedObjectContext {
                let entity = NSEntityDescription.entityForName("BlogPost", inManagedObjectContext: managedObjectContext)
                blogPost = BlogPost(entity: entity!, insertIntoManagedObjectContext: managedObjectContext)
            }
        }
        blogPost!.id = id //NSNumber(integer: id)
        blogPost!.title = title
        blogPost!.imageURLString = imageURLString
        blogPost!.date = date
        return blogPost
    }
    
    func getBlogPostFromCoreData(id: Int) -> BlogPost? {
        let fetchRequest = NSFetchRequest(entityName: "BlogPost")
        fetchRequest.predicate = NSPredicate(format: "id == \(id)", argumentArray: nil)
        var error: NSError?
        let blogPosts: [AnyObject]?
        do {
            blogPosts = try CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest)
        } catch let error1 as NSError {
            error = error1
            blogPosts = nil
        }
        if let error = error {
            print(error)
        }
        if let blogPost = blogPosts?.first as? BlogPost {
            return blogPost
        }
        return nil
    }
    
    func getCoreDataBlogPosts(completion: (blogPosts: [BlogPost]?) -> ()) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let fetchRequest = NSFetchRequest(entityName: "BlogPost")
            var error: NSError?
            var posts = CoreDataManager.sharedManager.managedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [BlogPost]
            if let error = error {
                print(error)
            }
            posts.sort {
                item1, item2 in
                return item1.date.compare(item2.date) == NSComparisonResult.OrderedDescending
            }
            completion(blogPosts: posts as [BlogPost]?)
        })
    }
    
    // MARK: - Table View
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blogPosts.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = "Cell"
        let cell: BlogPostTableViewCell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! BlogPostTableViewCell
        
        let blogPost = self.blogPosts[indexPath.row]
        cell.blogPostTitle.text = blogPost.title?
        if let imageData = blogPost.imageData {
            cell.blogPostImageView.image = UIImage(data: imageData)
        } else {
            self.fetchImageDataAndInsertIntoImageView(cell.blogPostImageView, blogPost: blogPost)
        }
        
        // LoadMoreBlogPosts
        if (indexPath.row > tableView.numberOfRowsInSection(indexPath.section) - 10) {
            self.getRemoteBlogPosts { response in
                self.createCoreDataObjectsFromJSONResponse(response)
            }
        }
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        self.performSegueWithIdentifier("showDetail", sender: indexPath)
    }
    
    func fetchImageDataAndInsertIntoImageView(imageView: UIImageView, blogPost: BlogPost) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            var imageData: NSData?
            if let imageURLString = blogPost.imageURLString {
                imageData = NSData(contentsOfURL: NSURL(string: imageURLString)!)
            }
            dispatch_async(dispatch_get_main_queue(), {
                if let imageData = imageData {
                    imageView.image = UIImage(data: imageData)
                    
                    blogPost.imageData = imageData
                    var error: NSError?
                    CoreDataManager.sharedManager.saveContext()
                }
            })
        })
    }
    
    // MARK: - Split view Controller
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        // true = Master
        // false = Detail
        let navigationViewcontroller: UINavigationController = secondaryViewController as! UINavigationController
        if let blogPostViewController: BlogPostViewController? = navigationController?.topViewController as? BlogPostViewController {
            if let blogPost = blogPostViewController?.blogPost {
                return false
            }
        }
        return true
    }
    
    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController) -> UIViewController? {
        // Return empty detail view if no blog post is selected
        return nil;
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "showDetail") {
            let navigationViewController: UINavigationController = segue.destinationViewController as! UINavigationController
            let blogPostViewController: BlogPostViewController = navigationViewController.topViewController as! BlogPostViewController
            let indexPath: NSIndexPath = sender as! NSIndexPath
            let blogPost = self.blogPosts[indexPath.row]
            blogPostViewController.blogPost = blogPost
        } else {
            print("Unknown segue identifier: \(segue.identifier)")
        }
    }
}
