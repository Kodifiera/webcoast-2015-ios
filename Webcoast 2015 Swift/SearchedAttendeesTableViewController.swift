//
//  SearchedAttendeesTableViewController.swift
//  Webcoast 2015
//
//  Created by Peter Larsson on 21/01/15.
//  Copyright (c) 2015 Peter Larsson. All rights reserved.
//

import UIKit
import RSTWebViewController


class SearchedAttendeesTableViewController: BaseAttendeeTableViewController, AttendeeTableViewCellDelegate {

    var searchedAttendees = [Attendee]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func attendeeTableViewCellDidPressedButton(button: UIButton) {
        if let website = button.titleLabel?.text {
            let webViewController = RSTWebViewController(address: website)
            webViewController.showsDoneButton = true
            let navigationController = UINavigationController(rootViewController: webViewController)
            presentViewController(navigationController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchedAttendees.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 136
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.Cell.identifier) as! AttendeeTableViewCell
        cell.delegate = self
        let attendee = self.searchedAttendees[indexPath.row]
        configureCell(cell, forAttendee: attendee)
        return cell
    }
}
